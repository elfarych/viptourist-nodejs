"use strict";
const tourServices = require("../services/tour");
const bot = require('../../utils/telegram-bot')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    /* ------------------------------------------------------------------------------------------------------------------ */
    /*                                                    BEFORE CREATE                                                   */
    /* ------------------------------------------------------------------------------------------------------------------ */
    async beforeCreate(data) {
      /* ------------------------------------------------------------------------------------------------------------------ */
      /*                                                  Add country name                                                  */
      /* ------------------------------------------------------------------------------------------------------------------ */
      if (data.city) {
        const cityId = data.city,
          country = await strapi.services.country.findOne({ id: cityId });
        if (country) {
          data.country = country.name;
        }
      }

      if (!data.vid) {
        const date = new Date();
        const vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`;
        data.vid = vid;
      }
    },
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ------------------------------------------------------------------------------------------------------------------ */
    /*                                                    BEFORE UPDATE                                                    */
    /* ------------------------------------------------------------------------------------------------------------------ */
    async beforeUpdate(params, data) {
      const tourToUpdate = await strapi.services.tour.findOne({ id: params?._id || data.id})

      if (tourToUpdate && !tourToUpdate.approved && data.approved) {
        if (!tourToUpdate.localizations?.length) {
          bot.translate.sendMessage('tours', params?._id || data.id, 'New tour: ')
          return tourServices.createTranslations(data, params);
        }
      }

      if(!tourToUpdate.next)  bot.admin.sendMessage('tours', params._id, 'Тур был обновлен: ')

    },

    async afterDelete(params, result) {
      try {
        params.localizations.forEach((item) => {
          strapi.services.tour.delete({ id: item._id });
        });
      } catch (e) {
        throw e;
      }
    },

    async afterCreate(result, data) {
      if (!data.localizations?.length) {
        bot.admin.sendMessage('tours', result.id)
      }

    }
  },
};
