"use strict";
const languages = require("../../utils/languages");
const axios = require("axios");
const translator = require("../../utils/translator");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  createTranslations: async function (data, params, iteration = 0) {
    const tour = JSON.parse(JSON.stringify(data));

    await languages.getLanguages()

    const langs = languages.languages.filter(
      (item) => item.locale !== tour.createdLanguage
    );
    const lang = langs[iteration];
    const currentCity = await strapi.services.city.findOne({
      id: tour.city,
    });

    /* ------------------------------------------------ Get translations ------------------------------------------------ */
    let translations = await translator.translateText({
      text: [data.name, data.description],
      target: lang.short,
    });
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ---------------------------------------------- Get current tour city --------------------------------------------- */
    try {
      tour.city = await axios
        .get(`http://${process.env.HOST}:${process.env.PORT}/cities`, {
          params: {
            vid: currentCity.vid,
            _locale: lang.locale,
          },
        })
        .then((response) => response.data[0].id);
    } catch (e) {
      throw e.message;
    }

    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ------------------------------------------------ Create translate ------------------------------------------------ */
    try {
      await axios
        .post(
          `http://${process.env.HOST}:${process.env.PORT}/tours/${params._id}/localizations`,
          {
            ...tour,
            name: translations?.length ? translations[0] : tour.name,
            description: translations?.length
              ? translations[1]
              : tour.description,
            next: true,
            locale: lang.locale,
          }
        )
        .then((response) => response);
    } catch (e) {
      throw e;
    }
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    if (iteration !== langs.length - 1) {
      return this.createTranslations(data, params, (iteration += 1));
    } else {
      return null;
    }
  },
};
