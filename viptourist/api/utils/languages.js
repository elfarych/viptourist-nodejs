const axios = require('axios')

module.exports = {
  languages: [],
  async getLanguages () {
    const context = this
    await axios.get(`http://${process.env.HOST}:${process.env.PORT}/i18n/locales`)
      .then(res => {
        context.languages = res.data.map(item => {
          return {
            locale: item.code,
            short: item.code.split('-')[0]
          }
        })
        console.log(context.languages.length + ' languages ready...')
      })
  }
}
