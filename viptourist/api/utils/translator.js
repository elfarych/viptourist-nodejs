const { Translate } = require("@google-cloud/translate").v2;

class Translator {
  key = "AIzaSyByQhNY4yuZa4GiMCSIZP8uIznijKFYf5Q";
  translate = new Translate({ key: this.key });

  async translateText({ text = [], target = "" }) {
    let translations = [];
    try {
      [translations] = await this.translate.translate(text, target);
    } catch (e) {
      throw e;
    }

    return translations;
  }
}

module.exports = new Translator();
