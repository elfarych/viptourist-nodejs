'use strict';
const qr = require('qrcode')
const services = require('../services/order')
const bot = require('../../utils/telegram-bot')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    beforeCreate (data) {
    },

    async afterCreate(results, data) {
      // Генерация qrcode
      await services.createQR(results, data)

      bot.finance.sendMessage('orders', results.id, 'Новый заказ: ')

      // Создание уведомления продавцу (гиду) о новой покупке
      await services.createNewOrderNotification(results, data)
    },

    async beforeUpdate(params, data) {
      await services.createSellerConfirmedNotification(params, data)
      try {
        const order = await strapi.services.order.findOne({ id: params._id })
        if (!order.canceled && data.canceled) {
          bot.finance.sendMessage('orders', params._id, 'Заказ отменен: ')
        }
      } catch (e) {
        throw e
      }
    }
  }
};
