"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {
      if (data.tour) {
        // Обновляем количество отзывов у тура
        const tourId = data.tour;
        const tour = await strapi.services.tour.findOne({id: tourId});
        const count = tour.reviews.length
        // ==================================

        // Обновляем рейтинг тура
        let tourRating;
        const tourReviews = await strapi.services.review.find({tour: tourId});
        if (tourReviews.length) {
          let reviewsRatingsSum = 0;
          tourReviews.forEach((item) => {
            reviewsRatingsSum += item.rating;
          });
          tourRating = reviewsRatingsSum / tourReviews.length;
        } else {
          tourRating = data.rating;
        }
        // =======================
        await strapi.services.tour.update(
          {id: tourId},
          {
            reviews_count: count,
            rating: tourRating,
          }
        );
      }

    },

    async beforeCreate(data) {
      // Добавляем имя и фото профиля к отзыву
      const profileId = data.profile;
      const profile = await strapi.services.profile.findOne({id: profileId});
      if (profile.photo || profile.photo_url) {
        data.photo_url = profile.photo?.formats?.small?.url || profile.photo?.url || profile.photo_url;
      }

      data.name = profile.name;
      // =====================================
    },

    async afterDelete(result, params) {
      // Обновляем количество отзывов у тура
      if (result.tour) {
        const tourId = result.tour.id;
        const tour = await strapi.services.tour.findOne({id: tourId});
        const count = tour.reviews.length

        await strapi.services.tour.update(
          {id: tourId},
          {
            reviews_count: count,
          }
        );
      }

      // ==================================
    },
  },
};
