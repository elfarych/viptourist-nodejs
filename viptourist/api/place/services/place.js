'use strict';
const languages = require("../../utils/languages");
const axios = require("axios");
const translator = require("../../utils/translator");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  async createTranslations (result, data) {
    const langs = languages.languages.filter(
      (item) => item.locale !== result.createdLanguage
    );

    for (const lang of langs) {
      let translations = await translator.translateText({
        text: [data.name],
        target: lang.short
      })

      try {
        await axios
          .post(`http://${process.env.HOST}:${process.env.PORT}/places/${result.id}/localizations`,
            {
              name: translations[0],
              locale: lang.locale
            })
      } catch (e) {
        throw e.message
      }
    }
  }
};
