'use strict';
const service = require('../services/place')
const bot = require('../../utils/telegram-bot')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {
      if (!data.localizations?.length) {
        await service.createTranslations(result, data)
        bot.admin.sendMessage(null, null, 'Новая заявка на добавление города http://admin.viptourist.club/#/places')
      }
    }
  }
};
