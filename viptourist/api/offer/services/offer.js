"use strict";
const languages = require("../../utils/languages");
const axios = require("axios");
const translator = require("../../utils/translator");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */
let notificationSendStarted = false

module.exports = {
  async getLanguages (offer) {
    await languages.getLanguages()
    return languages.languages.filter(
      (item) => item.locale !== offer.createdLanguage
    )
  },

  async createTranslations (data, params, iteration = 0) {
    const offer = JSON.parse(JSON.stringify(data));
    const langs = await this.getLanguages(data)
    const lang = langs[iteration];
    const currentTour = await strapi.services.tour.findOne({
      id: offer.tour,
    });

    /* ------------------------------------------------ Get translations ------------------------------------------------ */
    let translations = await translator.translateText({
      text: [
        offer.prerequisites,
        offer.prohibitions,
        offer.languages,
        offer.included,
        offer.not_included,
        offer.note,
      ],
      target: lang.short,
    });
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ---------------------------------------------- Get current tour city --------------------------------------------- */
    try {
      offer.tour = await axios
        .get(`http://${process.env.HOST}:${process.env.PORT}/tours`, {
          params: {
            vid: currentTour.vid,
            _locale: lang.locale,
          },
        })
        .then((response) => response.data[0].id);
    } catch (e) {
      throw e.message;
    }
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ------------------------------------------------ Create translate ------------------------------------------------ */
    try {
      await axios
        .post(
          `http://${process.env.HOST}:${process.env.PORT}/offers/${params._id}/localizations`,
          {
            ...offer,
            prerequisites: translations?.length
              ? translations[0]
              : offer.prerequisites,
            prohibitions: translations?.length
              ? translations[1]
              : offer.prohibitions,
            languages: translations?.length ? translations[2] : offer.languages,
            included: translations?.length ? translations[3] : offer.included,
            not_included: translations?.length
              ? translations[4]
              : offer.not_included,
            note: translations?.length ? translations[5] : offer.note,
            next: true,
            locale: lang.locale,
            profile: offer.profile,
          }
        )
        .then((response) => response);
    } catch (e) {
      throw e;
    }
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    if (iteration !== langs.length - 1) {
      return this.createTranslations(data, params, (iteration += 1));
    } else {
      return null;
    }
  },

  createVid (data) {
    if (!data.vid) {
      const date = new Date();
      data.vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`;
    }
  },

  async createApprovedNotification (params, data) {
    const offer = await strapi.services.offer.findOne({ id: params._id })

    if (!offer.approved && data.approved && !notificationSendStarted) {
      notificationSendStarted = true
      const profile = await strapi.services.profile.findOne({ _id: data.profile })
      const pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
        params: {
          _uid: profile.uid
        }
      }).then(res => res.data[0] || null)
      if (!pushProfile) return console.log('Profile for push not found...')

      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: "offerApproved",
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (e) {
        throw e
      }

      setTimeout(() => {
        notificationSendStarted = false
      }, 1000)
    }

  }
};
