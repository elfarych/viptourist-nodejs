"use strict";
const offerServices = require("../services/offer");
const bot = require("../../utils/telegram-bot");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    beforeCreate(data) {
      offerServices.createVid(data)
    },

    async afterCreate(result, data) {

      const id = data.tour;
      const tour = await strapi.services.tour.findOne({ id });
      const offers = await strapi.services.offer.find({ tour: tour.id });
      let minPrice;
      const sortedOffers = offers.sort((a, b) =>
        a.adult_price > b.adult_price ? 1 : -1
      );
      if (
        !sortedOffers.length ||
        data.adult_price < sortedOffers[0].adult_price
      ) {
        minPrice = data.adult_price;
      } else {
        minPrice = sortedOffers[0].adult_price;
      }

      await strapi.services.tour.update({ id }, {price: minPrice}
      );
      if (!data.localizations?.length) {
        bot.admin.sendMessage('offers', result.id)
      }
    },

    async beforeUpdate(params, data) {

      const offerToUpdate = await strapi.services.offer.findOne({id: params?._id || data.id,});
      if (offerToUpdate && !offerToUpdate.approved && data.approved) {
        if (!offerToUpdate.localizations?.length) {
          bot.translate.sendMessage('offers', params?._id || data.id, 'New tou: ')
          return offerServices.createTranslations(data, params);
        }
      }

      await offerServices.createApprovedNotification(params, data) // Создание уведомления гиду
    },

    async afterUpdate(result, params, data) {
      if(!result.next) bot.admin.sendMessage('offers', result.id, 'Оффер был обновлен: ')
    },

    async afterDelete(params, result) {
      try {
        params.localizations.forEach((item) => {
          strapi.services.offer.delete({ id: item._id });
        });
      } catch (e) {
        throw e;
      }
    },
  },
};
