"use strict";
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */
const services = require("../services/profile");
const bot = require('../../utils/telegram-bot')

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {
      await services.createProfileCopyForNotifications(result, data);
      bot.admin.sendMessage( data.is_tourist ? 'tourists' : 'guides', result.id)
    },

    async beforeUpdate(params, data) {
      await services.createProfileVerifiedNotification(params, data)
      try {
        const profile = await strapi.services.profile.findOne({ id: params._id })
        if (!profile.documents_urls?.urls && data.documents_urls?.urls) {
          bot.admin.sendMessage( data.is_tourist ? 'tourists' : 'guides', params.uid, 'Гид загрузил документы: ')
        }
      } catch (e) {
        throw e
      }
    },

    async afterUpdate(result, params, data) {
      await services.updateProfileCopyForNotifications(result, params, data);
    },
  },
};
