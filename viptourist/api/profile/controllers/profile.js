const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async findOne(ctx) {
    const { uid } = ctx.params;

    const entity = await strapi.services.profile.findOne({ uid });
    return sanitizeEntity(entity, { model: strapi.models.profile });
  }
}
