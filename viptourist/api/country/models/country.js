"use strict";
const services = require('../services/country')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    beforeCreate(data) {
      services.createVid(data)
    },

    async afterCreate(result, data) {
      await services.createTranslations(result, data)
    },
  },
};
