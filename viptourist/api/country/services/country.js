'use strict';
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */
const axios = require('axios')
const translator = require("../../utils/translator");
const languages = require("../../utils/languages");

module.exports = {
  async createTranslations(result, data) {

    if (!data.next) {
      await languages.getLanguages()

      const langs = languages.languages.filter(
        (item) => item.locale !== result.createdLanguage
      );

      const country = { ...data };

      for (const lang of langs) {
        country.locale = lang.locale;
        let translations = await translator.translateText({
          text: [country.name],
          target: lang.short
        })

        try {
          await axios
            .post(`http://${process.env.HOST}:${process.env.PORT}/countries/${result.id}/localizations`,
              {
                name: translations[0],
                locale: lang.locale,
                next: true
              })
        } catch (e) {
          throw e
        }
      }
    }
  },
  createVid (data) {
    if (!data.vid) {
      const date = new Date();
      data.vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`;
    }
  }
};
