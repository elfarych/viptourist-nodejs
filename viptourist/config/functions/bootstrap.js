'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

const languages = require("../../api/utils/languages");


module.exports = () => {
  setTimeout(() => {
    try {
      languages.getLanguages().then(() => {})
    } catch (e) {
      console.log(e)
    }
  }, 2000)

};
